# Hashed key = ad717a7504e9e5c42cf4b1d1578db4b6

---obfu-first---

# ########################
# #### KNOWN MAPPINGS ####
# ########################

ZJ'\x16\x02     Lambda
w\x0f'y\x01     array
g;g\t           iterator
okg(\x02        hasNext
tD&V            next
#;o             map
Y\t;\x04        List
c5\x1c          add
yNY{\x01        format
;XR,            agal
KS8&\x03        Tools
l*oN\x01        RegType
Y\x15~\x0f      RAttr
\x1d2\n+\x03    RConst
_(\x0ff\x03     RTemp
F\x0esI         ROut
|j;\x03\x01     RVar
\x187\x03\\\x03 getMaxTextures
K1\x01%         getProps
^\x157;         read
`&\ta           write
W_\x05R\x02     count
PVD'\x03        ofString
BRE\x1f         haxe
GY              io
{+NF\x01        Bytes
;\x17\bW        Unserializer
I\x05+          run
W\x01           b
\x04*+\\\x03    regStr
{ol             Std
d)S\x10\x02     string
m\x01           t
YaJi            index
y$L\x19\x03     access
)'1F            comp
tE'E\x03        offset
3\x06bE         swiz
/\x04UT\x02     opStr
\x1cx\x18c      Type
l%\x01F         enumParameters
P3\x1d\x01     enumConstructor
@;&x\x01        params
\t@A            tag
{FUd\x01        flags
B\x16F)\x01     TexFlag
\x1bK_          T2D
H8\f[\x03       TCube
\x04u5          T3D
~c\x0e^\x02     TMipMapDisable
z(R'\x03        TMipMapNearest
Y\x01Cw\x02     TMipMapLinear
\x1c\x14n0\x02  TWrap
ri3R\x02        TClamp
ub{\x10\x01     TFilterNearest
w)|\x18\x03     TFilterLinear
x'24\x02        TLodBias
|Bp\x05         Opcode
@\x20K|         OMov
\x18)eZ         OAdd
dL\x06;         OSub
C&Ww            OMul
m_&{            ODiv
wM`\f           ORcp
V/\x04l         OMin
x|D             OMax
_AvV            OFrc
3pwR            OSqt
/}\x04r         ORsq
\x03f\x12|      OPow
\x13$q         OLog
yt\x1bR         OExp
L;\\|           ONrm
\n\x13M\x1b     OSin
\nM;;           OCos
{B\x10\x06\x01  OCrs
?aF\x17         ODp3
\x0f\x20\bM     ODp4
\x1d\x11KH      OAbs
7;I\x03         ONeg
o_{\x02         OSat
\x04\x01{P      OM33
z\x1cy&         OM44
&x\x05         OM34
Y,=            OKil
,\x15\x109      OTex
';jK            OSge
\x01\x19'\x06\x01       OSlt
\x1b(\x17\r\x01 OUnused
6LRE            OSeq
X\x06);         OSne
\x16\x01        C
c\x01           X
\x06\x01        Y
\x1c\x01        Z
N\x01           W
\x1a\x01p       jpg
=4\x1c\x05      BitString
@\n\x17         len
\x16ip          val
=mDG\x03        Writer
\x18/34\x03     Output
\x06C[*\x01     YTable
PeXV\x03        UVTable
v%0\x1d\x03     fdtbl_Y
Gj\x1c`\x02     fdtbl_UV
Q$;\x03        IntHash
Fv\x115\x02     bitcode
c=4;\x01        category
m\x07A\x1b\x02  byteout
_^ks\x03        bytenew
yg\x17)         bytepos
aG7(            YDC_HT
C5;4\x01        UVDC_HT
o\x04\x0e{\x01  YAC_HT
(/bv\x02        UVAC_HT
B\x19|          YDU
%\x14          UDU
-'Z             VDU
K\x01           DU
9W\x1ec\x01     initZigZag
\x1f`\x03d\x01  initLuminance
$M~v            initChrominance
MX\x06z\x01     initHuffmanTbl
(\x19\nY        initCategoryNumber
foX\x1b         ZigZag
K\x01HU         initQuantTables
X8_y\x01        std_dc_luminance_nrcodes
\\\x20'f        std_dc_luminance_values
Jx[\b\x01       std_ac_luminance_nrcodes
c6i\t\x03       std_ac_luminance_values
blV\x1e\x03     strIntsToBytes
%*\x13*\x01     computeHuffmanTbl
Wl%@            std_dc_chrominance_nrcodes
`n\f\x1c        std_dc_chrominance_values
~\x03s/\x01     std_ac_chrominance_nrcodes
}Y\tG           std_ac_chrominance_values
i\fV            set
\x07h\x07n\x02  fDCTQuant
9u~n\x01        writeByte
(+?\x0e\x02     RGB2YUV
\t\x13]#        writeWord
\x12|_\x14\x01  writeSOS
\x10lD/\x03     writeSOF0
N'p\x01        writeDQT
j4\x13N\x03     writeDHT
\x12l`-\x02     writeBits
QrOI\x03        writeAPP0
J;cp\x01        quality
\x01\x0fL\x01   width
0e4F\x02        height
\x1bM\x0eq      pixels
Z?;D\x01        processDU
\b\x068         get
\x0fv\x16\x01\x03       BytesBuffer
hkab            parseInt
\x0e\x13\x14C\x01       getBytes
\x19w3\x05\x01  hxsl
J\x01\f4\x03    Shader
mR\x10          buf
+\x01           p
;\x01           c
q\bK\x1a\x02    FORMATS
1['k            bindReg
p%\x16q\x01     regIndex
*Vgl            bufSize
(ol)\x01        getVertexData
6)M?\x01        getFragmentData
I\x03b\x03      unbindTex
rKO\x1a\x01     texture
J?,\x1b\x02     unbind
&\x18)\f        send
n0\\`\x02       select
\x07\x1dP\x07\x01       draw
}\x17eX         bind
t3\x11Y         bindInit
=L\x01*\x03     bindDone
\nG((\x03       tools
$dRa\x03        Image
\x12d;X\x02     getBytesARGB
e^\x12v         ofData
\x0e'\x02K\x02  makeBitmapARGB
t=1A\x01        length
'\x01           h
l\x01           q
pC`~\x01        remove
&L\x10          pop
z\x0efH\x02     DEFAULT_RESOLVER
\b\x01;j\x02    BASE64
ePaP\x01        CODES
M;\x16C\x01     resolver
\n\x0er         pos
y;\x15Z\x01     setResolver
^~C;\x02        scache
$\x1dl\x01      cache
I\x1dB|\x01     initCodes
~P\x0f\x1c      unserialize
\x07\x15M;\x03  unserializeObject
Ac              is
#]JF\x03        unserializeEnum
$(\x0b)\x01     readDigits
$M\x10X         createEnum
\x1dT;         resolveClass
g]{\t\x01       createEmptyInstance
[#H2            hxUnserialize
\x13,-\x0f\x02  parseFloat
U![T\x02        resolveEnum
\x03guO         getEnumConstructs
L@\x1eX\x03     alloc
\\\x01}\\\x01   fromString
\t%7?\x01       StringTools
(\x07\x10f      urlDecode
rh`(            EReg
z\x1b9V\x01     matched
n58\f           match
$niH            Hash
\x03ouJ         keys
sYyK\x03        getClassName
=+s\n\x03       getEnumName
\x1bx\x03\\     createInstance
J\x14t2\x01     Reflect
\x10a)\x13\x02  field
*Zr_\x03        typeof
PKRB\x02        ValueType
ZR3`\x03        TNull
STj\x07\x01     TInt
\fVd~\x02       TFloat
C0a)\x02        TBool
\x15\f];\x02    TObject
U\x01V         TFunction
9\x20g'         TClass
\x0bV)\x13\x02  TEnum
\rY;b\x01       TUnknown
\x01c?g\x03     dispose         # note: flash.display3D.*.dispose's aren't obfuscated
3f\x01          ref
'k              it
\x14+B?         exists
'j\x1bI\x03     fields
\x030p\x16\x03  isFunction
\x13xvm\x01     compare
e4[6            tora
ha!\x06         Code
m\x0f_\x01\x03  CFile
\x15Bc)         CUri
ii\x14s\x01     CClientIP
2|q/\x02        CGetParams
\x13[\x16J      CPostData
\x04]\x1e!\x01  CHeaderKey
,\x02;|         CHeaderValue
,'o,\x03        CHeaderAddValue
\x11s\r\x0b\x01 CParamKey
\x19\x20Z\r\x03 CParamValue
+\x15HE         CHostName
8)GQ\x02        CHttpMethod
EOuP\x01        CExecute
Z)\x02D\x02     CError
\x1b\x1e,u\x03  CPrint
\x05Q$m         CLog
z{-D\x03        CFlush
UoJ(\x02        CRedirect
\f#-#\x01       CReturnCode
(\x0bR\x02     CQueryMultipart
\x10n*9\x02     CPartFilename
E\n\x03d\x03    CPartKey
\x10\x052j\x02  CPartData
9dN[\x02        CPartDone
q\x1f\x1b\x03\x03       CTestConnect
bBE\x1d\x01     CListen
!\x03\x0f`\x02  CHostResolve
]sTZ            Protocol
\n~W\x0e\x03    onError
G\x01((\x03     onDisconnect
@s+G\x02        onData
OS{\x19         headers
gm@\t           host
\x0f\x01\x04\x01        uri
@'\x04\x1c      port
8\t!X           wait
q\nOy           reset
2H\x1c+\x01     onSocketData
Jmlz            sock
\x1dJN\x15      lastMessage
GS`n\x01        error
^v4O            dataLength
seqf\x01        onConnect
])l^\x03        urlEncode
*G#             key
H1'\x12\x02     value
\n\n\x12\r\x02  onClose
\f'r[\x02       flash                   # note: flash isn't obfuscated when referencing Flash's native classes
\x15-\x14l\x01  connect
lk\x1b/         addParameter
rm\x16s         addHeader
$\x16O?\x01     remoting
!;'x\x01        Connection
i\x1bD8\x03     ExternalConnection
|j_+\x01        connections
\x1f,ZG         escapeString
\bl\x01%\x02    doCall
G`$f\x03        jsConnect
SME2            resolve
~\x1d0\x05      Serializer
k'T\n\x02       shash
;3t\x15\x01     scount
!\x18)d         serializeString
[Y\x11S\x03     serializeRef
t(/\x06\x01     serializeFields
0[\x1e\x12\x02  serializeException
Ftya            serializeClassFields
(K=\x05         serialize
\x20;mQ\x01     AsyncConnection
MM#b            setErrorHandler
+\x19IY\x01     Context
Z9YC\x02        objects
{iO\x0e\x01     addObject
AQx             obj
Vec             rec
)\x06           id
;4[+            name
\x16xC          ctx
*_E!\x02        queue
({\x03I\x03     timer
z$\x14\x1e\x03  FlashJsConnection
'(]`\x01        Error
{SD|            Blocked
\x01k#\x1b\x01  Overflow
Y/eb            OutsideBounds
t\\#\x1d\x01    Custom
\\)[I\x02       BytesOutput
\x0bKX\x03     writeString
\x02\x0f\r\x06\x03  writeBytes
WS`9            Http
4\x16\x0e       url
X3I\x1b         onStatus
\x20'!\n\x03    setParameter
|3\n\r          request
Qb!\x02        loader
Ap\r            Md5
\x12\x0bN       rol
L\x14*\x12\x02  encode
\x03\x01\x05#\x01   bitXOR
\x10cjg         bitOR
\tL-           bitAND
*jO\t\x01       addme
)(H             cmn
\x045           ff
h\x06           gg
\bw             hh
\x14=           ii
\x10!xf\x02     doEncode
$\x1aC          Log
eFfV\x02        trace
\x18*\x202      setColor
c\x09@\\\x01    clear
4tt@\x03        Timer
-\x0b\x1e8\x01  delay
SnW8            stop
R\x01           f
\x02\x1aj\x05   SHA1
aBAZ            init__
nS9Z            Boot
26&6            skip_constructor
of*g            lastError
_5i\f\x03       useCache
eA7i            USE_CACHE
jx`g            useEnumIndex
(\x1b$5\x02     USE_ENUM_INDEX
x4t\x1d\x02     str2blks
%Ja(\x02        Generator
'^\x18v\x01     Photo
_\x01           g
4\x01           x
2\x01           y
e\x01           z
S-9\x04         Kube
EU              mt
\x1ay7          m3d
Df              UV
\x1a\x01        u
%\x01           v
v\x01           r
\x06[\x0e%      random
\x1b^           me
\x1dT}'\x01     Codec
\x19_'o         StringBuf
q_\x11?         load
\x07\x14b       bmp
/$C\x17\x01     generate
\x1e`\x167\x01  wantedFPS
\\\x0b%U\x01    maxDeltaTime
$y(\x04\x01     oldTime
\x0fQ\x04\x1b\x03 tmod_factor
\x0epE`         calc_tmod
)\x16j(         tmod
\t\rm\x20\x01   deltaT
\x1d\x0bGB\x01  frameCount
\r\x10g\x0e\x03 paused
=A              Key
&%\x1aE\x01     fl_initDone
\\\x0b,k\x03    kcodes
dB$;\x02        ktime
\x1aUm          Lib 
)}6P\x01        current
\x04,WT\x03     onKey
\x16\x1e\x12\x13\x02 onEnterFrame
q4i\x14         event
bD;\t\x02       isDown
HVwO\x01        isToggled
,L4I            Init
4f;\x1e\x02     check
#w\x0f6\x01     Public
hyRY\x03        lines
y(SK\x03        enum_to_string
\x149}\x10\x03  getTrace
\x05U{$\x01     fileName
p'h\x02\x03     start
jy'I\x03        doInitDelay
)\tac\x03       Manager
\x010^\x0b\x02  GOWater
V6~2\x02        GOLava
dQ\x06,\x01     takePhoto
WR{\x12\x03     savePhoto


# List of kubes

au$P\x02        BlockKind

_L\x20S         BFixed                  # 00 - Béton
/n(\n           BWater                  # 01 - Eau
svMu\x01        BSoilTree               # 02 - Terre Brune
&=GL\x03        BSoilTree1              # 03 - Bois Bouleau
;E-P\x03        BSoilTree2              # 04 - Feuilles Bouleau
W}jC\x01        BAutumnTree             # 05 - Terre d'Automne
f('\x05\x01     BAutumnTree1            # 06 - Bois d'Automne
\x07/)1\x03     BAutumnTree2            # 07 - Feuilles d'Automne
\x20\x1bnn\x02  BHighTree               # 08 - Terre Sombre
0#(\x1e\x03     BHighTree1              # 09 - Bois Sombre
+;O}\x03        BHighTree2              # 10 - Feuilles Sombres
D0_P            BHighTree3              # 11 - Herbes Sombres
/\x1d[d\x02     BClouds                 # 12 - Terre Bleue
P\x14;\b\x03    BClouds1                # 13 - Coeur de Nuage
#\x19QB\x02     BClouds2                # 14 - Nuage
\x14b\\\x1d\x02 BSphereTree             # 15 - Bois Clair
5\x0f4\x16\x02  BSphereTree1            # 16 - Feuilles Roses
\x05\x18'y\x03  BLargeTree              # 17 - Racines
\x12sp\x01     BLargeTree1             # 18 - Bois Géant
\x07\x1eV;\x03  BLargeTree2             # 19 - Feuilles Géantes
'P}\x1a         BSnowSapin              # 20 - Terre-Neige
w2dP\x03        BSnowSapin1             # 21 - Bois Sapin
(%\\)\x02       BSnowSapin2             # 22 - Feuilles-Neige
wFU*            BJungle                 # 23 - Jungle
c\x17;M         BJungle1                # 24 - Lianes Sombres
'XyJ\x03        BJungle2                # 25 - Lianes Claires
2pr'            BJungle3                # 26 - Lianes Pourries
)\x01x$         BCaverns                # 27 - Mur
6\x14}V\x01     BCaverns1               # 28 - Roc
\b]`~\x01       BCaverns2               # 29 - Pilier
)}'\x18\x01     BField                  # 30 - Prairie
\x04\x1foL\x02  BField1                 # 31 - Herbes Claires
A\x1cM\x03     BField2                 # 32 - Herbes
\x20z+Z         BSwamp                  # 33 - Marais
??!\t\x02       BSwamp1                 # 34 - Bois Marais
_w`x\x01        BSwamp2                 # 35 - Feuilles Marais
\x11t0p         BSwamp3                 # 36 - Boue
dc!w\x02        BSoilPeaks              # 37 - Roche Froide
\x03\x1bwv\x01  BSoilPeaks1             # 38 - Roche Volcanique
zf'S\x03        BPilarPlain             # 39 - Cendres
\x01PS\x01     BPilarPlain1            # 40 - Grès
9`\x18^\x01     BPilarPlain2            # 41 - Totem Antique
FF\x01a\x01     BFlowerPlain            # 42 - Pâturages
%;\r1\x01       BFlowerPlain1           # 43 - Fleur Blanche
\x14{\b8\x03    BFlowerPlain2           # 44 - Fleur Rose
{vO\x17         BFlowerPlain3           # 45 - Stonehenge
S\t\x1a5        BSavana                 # 46 - Savane
C4'K            BSavana1                # 47 - Arbuste Sec
SV\x17\x1c\x01  BSavana2                # 48 - Zèbre
;ay-\x03        BSavana3                # 49 - Eléphant
tZ/            BDesert                 # 50 - Sable
\x18\x07\x189\x03       BDesert1        # 51 - Cactus
o3(?\x03        BFruit                  # 52 - Gland
TB7J            BLava                   # 53 - Lave
=;\x1d\x1b\x03  BDolpart                # 54 - Sombre
iv2n\x01        BDolmen                 # 55 - Dolmen
?O\x1d6\x02     BFlying                 # 56 - Méka
\n-\fz\x01      BFlying1                # 57 - Métal
=\x10\x10\x1b   BFlying2                # 58 - Acier
@p\x20\x02     BKoala                  # 59 - Koala
\x20QR\x12\x01  BInvisible              # 60 - Invisible
I\x0b%R\x02     BAmethyste              # 61 - Améthyste
;Q#8\x02        BEmeraude               # 62 - Emeraude
\x01C\x02J\x03  BRubis                  # 63 - Rubis
fGa\x17\x01     BSaphir                 # 64 - Saphir
\x12=\x06{      BFog                    # 65 - Brouillard
\x03A\x11\x18\x02       BShade          # 66 - Ombre
`RS\x01         BLight                  # 67 - Lumière
sw\x05\n\x01    BGold                   # 68 - Or
\x11\x03rW      BMessage                # 69 - Forum
_(\\p\x03       BTeleport               # 70 - Téléport
4C\x01}\x03     BJump                   # 71 - Ressort
`\x01}6\x01     BChest                  # 72 - Kofre
_]\rb\x03       BMulticol               # 73 - Rubix
|{D\x02        BMTwin                  # 74 - Motion Twin
68\x1f\x02\x01  BCrate                  # 75 - Caisse
7$Ow\x02        BNoel                   # 76 - Noël


# List of islands

0\x17@x         Kind

6\x12J\x17\x03  KSoilTree               # 00 - Forêt Bouleau
\x11+r\x01      KAutumnTree             # 01 - Forêt d'Automne
#\x20|p\x03     KHighTree               # 02 - Hautes Cîmes
\x1b\x1b]#\x01  KClouds                 # 03 - Montagne Bleue
C\f\x03'\x02    KSphereTree             # 04 - Shinsekai
qT\x13+\x02     KLargeTree              # 05 - Séquoia
+\x20=Y         KSnowSapin              # 06 - Montagne Enneigée
|\x1c2*\x02     KJungle                 # 07 - Jungle
V\x1bxt         KCaverns                # 08 - Grotte
(|ki\x03        KField                  # 09 - Prairies
rk9'\x01        KSwamp                  # 10 - Marais
A]JR\x02        KSoilPeaks              # 11 - Volcan
pJO;\x02        KPilarPlain             # 12 - Antenne
_ao7\x01        KFlowerPlain            # 13 - Pâturages
\x20L\x05B\x02  KSavana                 # 14 - Savane
v\x1c)&         KDesert                 # 15 - Désert
_bn\\\x02       KFlying                 # 16 - Méka


# Server commands (enum _Cmd)
# Unknown mappings are postfixed with ___

rX~\x04\x02     CWatch                  # 00
OF;K\x01        CGenLevel               # 01
\x0b!SM\x03     CSetBlocks              # 02
;\fF\x01\x02    CSavePos                # 03
H8`d            CAdminDelete            # 04
)\x1ae$         CLoad                   # 05
t/eU\x01        CRegen                  # 06
-k\x1ek         CGameOver               # 07
\x11e$\x14\x01  CDoCheck                # 08
s9\x1a\x16\x01  CDropKubes              # 09
e\x01aL\x02     CPing                   # 10
av|K            CSavePhoto              # 11
\x10'\x20\x03   CActiveKube             # 12
}Z51\x01        CUpdateTuto             # 13
'5+%            CGetStatus              # 14
vH;\x01\x03     CTeleport               # 15
3u[F\x01        CUndo                   # 16
\x15~d@\x03     CCountBlocks            # 17
o0Se            CFix                    # 18


# Server responses (enum _Answer)

FL(|\x01        ABlocks                 # 00
Ah@\x15         ASet                    # 01
\x206PK\x03     APosSaved               # 02
#ID\x1f         AMap                    # 03
,\x0eg\x04\x01  AGenerate               # 04
}ggS\x03        ARedirect               # 05
ZMF4\x02        AMessage                # 06
@4M2\x02        ANothing                # 07
(#@+\x02        AValue                  # 08
@\x18(\\\x03    AShowError              # 09
zcgp\x02        APong                   # 10
ORL\b\x01       ASetMany                # 11


# ##########################
# #### UNKNOWN MAPPINGS ####
# ##########################

\n22#           hexChar___
\x185\x1er\x01  HEX_CHARS___
|\x03j{\x02     stringToArray___
P\x16HC         walking___
c(\x01(\x01     drawSky___
;;DE\x03        isOnWater____
c7i\x13         makeOrganicTree___
s)UH            makePyramidTree___
Ni\x06l         SeedHasher___
=\x1dw`         output___
\x01\x07]\x1b\x01   putValue___
\x1cu-\x01     uploadPhoto___
\x01Z\x03\x14\x03   takePhoto2___
[0\x04s\x03     IslandsInfo___
\x0f7+         fillArray___
\x10\x01        type___                 # maybe t (for type)?
RoVQ            soilForType___
X\x16HD\x02     lavaForType___
]\x03T\x04\x03  heightForType___
Y\nX\x15\x03    probaForType___
\x14~8*\x03     decorationsForType___
Z\x1ayk         islandTypeArray___
|\x1ap}         veinsTypeArray
|J9\x1f\x02     islandsInfo___
4\x12[+\x02     ZoneData___
(lG\x1e\x01     getInBounds___
7Kf             isSolid___
Fix\x11         packCoordinates___
bM\x16[\x01     isOutOfBounds___
\x1b\x15        textField___
N8\x14)\x01     base64Chars___
o\x18\x16_      isLocal___
\x14\x03jv\x03  typeResolvers___
\x065e4\x02     addTypeResolver___
d\x0f\x14\x0f\x02   getLoaderVar___
4*x(\x02        openConnections___
\x1cb\x01d\x01  keyExtra___
7;\x1c\x18\x01  KeyExtra___
)[K8\x03        unserializeLoaderVar___
\x01wyT\x01     protocol___
w\x1dF\x05      hasCapability___
{/4%\x03        killPlayer___
